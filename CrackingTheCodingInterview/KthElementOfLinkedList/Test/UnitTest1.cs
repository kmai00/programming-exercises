using KthElementOfLinkedList;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        LinkedListRunner _Sut;

        [SetUp]
        public void Setup()
        {
            _Sut = new LinkedListRunner();
        }

        [Test]
        public void Test1()
        {
            var list = new LinkedListNode
            {
                Data = 1
            };

            var result = _Sut.PrintKthElementToLast(list, 1);
            Assert.AreEqual(1, result);
            Assert.AreEqual(1, _Sut.Answer);
        }

        [Test]
        public void Test2()
        {
            var node_2 = new LinkedListNode
            {
                Data = 2
            };

            var node_1 = new LinkedListNode
            {
                Data = 1,
                Next = node_2 
            };

            var result = _Sut.PrintKthElementToLast(node_1, 2);
            Assert.AreEqual(2, result);
            Assert.AreEqual(1, _Sut.Answer);
        }
    }
}