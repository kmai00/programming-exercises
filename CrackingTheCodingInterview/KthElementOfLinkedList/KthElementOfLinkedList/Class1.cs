﻿using System;

namespace KthElementOfLinkedList
{
    public class LinkedListNode
    {
        public int Data;
        public LinkedListNode Next;
    }

    public class LinkedListRunner
    {
        public int Answer;

        public LinkedListRunner() {
            Answer = -1;
        }

        public int PrintKthElementToLast(LinkedListNode head, int k)
        {
            if (head == null)
            {
                return 0;
            }
            var index = PrintKthElementToLast(head.Next, k) + 1;
            if (index == k) {
                Answer = head.Data;
            }
            return index;
        }
    }
}
