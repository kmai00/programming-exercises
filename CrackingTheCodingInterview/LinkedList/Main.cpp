//Linked List Exercises

#include <iostream>
#include "LinkedList.h"

using namespace std;

int main() {
	LinkedList *linkedList = new LinkedList(1);
	linkedList->printLinkedList();
	linkedList->insertValue(2);
	linkedList->printLinkedList();
	linkedList->insertValue(0);
	linkedList->printLinkedList();
	linkedList->insertValue(3);
	linkedList->printLinkedList();
	linkedList->insertValue(10);
	linkedList->printLinkedList();
	linkedList->insertValue(6);
	linkedList->printLinkedList();

	linkedList->deleteValue(0);
	linkedList->printLinkedList();
	linkedList->deleteValue(10);
	linkedList->printLinkedList();
	linkedList->deleteValue(3);
	linkedList->printLinkedList();
	linkedList->deleteValue(12);
	linkedList->printLinkedList();
	cin.ignore();
}