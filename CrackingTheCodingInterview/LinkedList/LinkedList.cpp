#include "LinkedList.h"
#include <iostream>

using namespace std;

LinkedList::LinkedList(int value) {
	head = new Node(value);
}

void LinkedList::insertValue(int value) {
	Node *current = head;
	Node *previous = NULL;
	while (current != NULL && current->value < value) {
		previous = current;
		current = current->next;
	}
	Node *newNode = new Node(value);
	if (previous == NULL) { //Beginning
		newNode->next = current;
		head = newNode;
	}
	else if(current != NULL){ //Mid
		previous->next = newNode;
		newNode->next = current;
	}
	else { //End
		previous->next = newNode;
	}
}

void LinkedList::printLinkedList() {
	Node *current = head;
	while (current != NULL) {
		printf("%d, ", current->value);
		current = current->next;
	}
	printf("\n");
}

void LinkedList::deleteValue(int value)
{
	Node *current = head;
	Node *previous = NULL;
	while (current != NULL && current->value != value) {
		previous = current;
		current = current->next;
	}
	if (current == NULL) {
		printf("Cannot find value in Linked List\n");
	}
	else if(previous != NULL) { //Mid
		previous->next = current->next;
	}
	else { //Head
		head = head->next;
	}
}
