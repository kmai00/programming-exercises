#ifndef  LINKEDLIST_H
#define LINKEDLIST_H

class Node {
public:
	Node(int value) {
		this->value = value;
	}
	int value;
	Node* next;
};

class LinkedList {
private:
	Node *head;
public:
	LinkedList(int value);
	void insertValue(int vlaue);
	void printLinkedList();
	void deleteValue(int value);
};

#endif 