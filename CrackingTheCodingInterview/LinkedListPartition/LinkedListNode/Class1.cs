﻿using System;

namespace LinkedListNode
{
    public class LinkedListNodeClass
    {
        public int data;
        public LinkedListNodeClass Next;
    }

    public class ParitionClass
    {
        public LinkedListNodeClass Partition(LinkedListNodeClass node, int x)
        {
            LinkedListNodeClass head = node;
            LinkedListNodeClass tail = node;

            while (node != null)
            {
                var next = node.Next;
                if (node.data < x)
                {
                    node.Next = head;
                    head = node;
                }
                else {
                    tail.Next = node;
                    tail = node;
                }
                node = next;
            }

            tail.Next = null;
            return head;
        }
    }
}
