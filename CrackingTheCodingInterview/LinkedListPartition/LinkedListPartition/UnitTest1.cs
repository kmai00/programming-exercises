using LinkedListNode;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        ParitionClass _Sut;

        [SetUp]
        public void Setup()
        {
            _Sut = new ParitionClass();
        }

        [Test]
        public void LessThan()
        {
            var link_3 = new LinkedListNodeClass
            {
                data = 3,
            };
            var link_2 = new LinkedListNodeClass
            {
                data = 2,
                Next = link_3
            };
            var link_1 = new LinkedListNodeClass
            {
                data = 1,
                Next = link_2
            };

            var result = _Sut.Partition(link_1, 2);
        }
    }
}