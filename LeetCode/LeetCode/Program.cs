﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Given an array of integers, find two numbers such that they add up to a specific target number.

The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are not zero-based.

You may assume that each input would have exactly one solution.
*/

namespace LeetCode {
    class Program {
        static void Main(string[] args) {
            var nums = new int[] { 2, 1, 9, 4, 4, 56, 90, 3 };
            var answer = TwoSum(nums, 8);

            Console.WriteLine(answer[0] + " " + answer[1]);
            Console.ReadLine();
        }

        static int[] TwoSum(int[] nums, int target) {
            var twoSum = new int[2];
            Hashtable hash = new Hashtable();
            int index1 = -1, index2 = -1;
            for (var i = 0; i < nums.Length; i++) {
                var compliment = target - nums[i];
                if (hash.ContainsKey(compliment)) {
                    index1 = i;
                    index2 = (int)hash[compliment];
                    break;
                } else {
                    hash[nums[i]] = i;
                }
            }


            twoSum[0] = index2;
            twoSum[1] = index1;
            twoSum[0] += 1;
            twoSum[1] += 1;
            return twoSum;
        }
    }
}