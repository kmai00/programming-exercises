using LongestPalindromicString;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        LongestPalindromicSolution _Sut;

        [SetUp]
        public void Setup()
        {
            _Sut = new LongestPalindromicSolution();
        }

        [Test]
        public void Test1()
        {
            var input = "babad";
            var expected = "bab";

            var result = _Sut.LongestPalindrome(input);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Test2()
        {
            var input = "cbbd";
            var expected = "bb";

            var result = _Sut.LongestPalindrome(input);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Test3()
        {
            var input = "a";
            var expected = "a";

            var result = _Sut.LongestPalindrome(input);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void Test4()
        {
            var input = "ac";
            var expected = "a";

            var result = _Sut.LongestPalindrome(input);
            Assert.AreEqual(expected, result);
        }
    }
}