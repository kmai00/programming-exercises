﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Pretty bad

namespace AddTwo_Algo {

    public class ListNode {
        public int val;
        public ListNode next;
        public ListNode(int x) { val = x; }
    }

    class Program {
        static void Main(string[] args) {
            ListNode node1 = new ListNode(1);
            node1.next = new ListNode(9);
            node1.next.next = new ListNode(9);
            node1.next.next.next = new ListNode(9);
            node1.next.next.next.next = new ListNode(9);
            node1.next.next.next.next.next= new ListNode(9);
            node1.next.next.next.next.next.next = new ListNode(9);
            node1.next.next.next.next.next.next.next = new ListNode(9);
            node1.next.next.next.next.next.next.next.next = new ListNode(9);
            node1.next.next.next.next.next.next.next.next.next = new ListNode(9);

            ListNode node2 = new ListNode(9);

            ListNode result = AddTwoNumbers(node1, node2);
        }

        public static ListNode AddTwoNumbers(ListNode l1, ListNode l2) {
            long value1 = getLong(l1);
            long value2 = getLong(l2);
            long total = value1 + value2;
            char[] total_string = total.ToString().ToCharArray();
            int val;
            Int32.TryParse(total_string[total_string.Length - 1].ToString(),out val);
            ListNode result = new ListNode(val);
            ListNode temp = result;
            for (int i = total_string.Length - 2; i >= 0; i--) {
                if (temp.next == null) {
                    Int32.TryParse(total_string[i].ToString(), out val);
                    temp.next = new ListNode(val);
                    temp = temp.next;
                }
            }
            return result;
        }


        public static long getLong(ListNode node) {
            string number ="";
            for(var temp = node; temp != null; temp = temp.next) {
                number += temp.val;
            }
            char[] arr = number.ToCharArray();
            Array.Reverse(arr);
            number = new string(arr);
            long val;
            val = Convert.ToInt64(number);
            return val;
        }
    }
}
