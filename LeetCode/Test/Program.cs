﻿using ExcelColumnClass;
using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var t = new ExcelColumn();

            // multiple of 26
            for (int i = 0; i <= 676; i++)
            {
                if (i % 26 == 0)
                {
                    Console.WriteLine($"{i} : {t.GetColumn(i)}");
                }
            }
        }
    }
}
