﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LongestPalindromicString
{
    public class LongestPalindromicSolution
    {
        public string LongestPalindrome(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            //Choose center
            var start = 0;
            var end = 0;

            for (var i = 0; i < s.Length; i++)
            {
                var length_1 = ExpandCenter(s, i, i);
                var length_2 = ExpandCenter(s, i, i + 1);
                var length = Math.Max(length_1, length_2);

                if (length > (end - start) + 1)
                {
                    start = i - (length - 1) / 2;
                    end = i + length / 2;
                }
            }

            return s.Substring(start, (end - start + 1));
        }

        private int ExpandCenter(string s, int left, int right)
        {
            var leftIndex = left;
            var rightIndex = right;

            while (leftIndex >= 0 && rightIndex < s.Length && s[leftIndex] == s[rightIndex])
            {
                leftIndex--;
                rightIndex++;
            }

            return rightIndex - leftIndex - 1;
        }
    }
}
