using ExcelColumnClass;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        ExcelColumn _Sut;

        [SetUp]
        public void Setup()
        {
            _Sut = new ExcelColumn();
        }

        [Test]
        public void Test_A()
        {
            var expected = "A";
            Assert.AreEqual(expected, _Sut.GetColumn(0));
        }

        [Test]
        public void Test_B()
        {
            var expected = "B";
            Assert.AreEqual(expected, _Sut.GetColumn(1));
        }

        [Test]
        public void Test_Z()
        {
            var expected = "Z";
            Assert.AreEqual(expected, _Sut.GetColumn(25));
        }

        [Test]
        public void Test_AA()
        {
            var expected = "AA";
            Assert.AreEqual(expected, _Sut.GetColumn(26));
        }

        [Test]
        public void Test_AB()
        {
            var expected = "AB";
            Assert.AreEqual(expected, _Sut.GetColumn(27));
        }

        [Test]
        public void Test_BA()
        {
            var expected = "BA";
            Assert.AreEqual(expected, _Sut.GetColumn(52));
        }

        [Test]
        public void Test_BB()
        {
            var expected = "BB";
            Assert.AreEqual(expected, _Sut.GetColumn(53));
        }


        [Test]
        public void Test_AAA()
        {
            var expected = "AAA";
            Assert.AreEqual(expected, _Sut.GetColumn(702));
        }


        [Test]
        public void Test_AAAA()
        {
            var expected = "AAAA";
            Assert.AreEqual(expected, _Sut.GetColumn(18278));
        }
    }
}